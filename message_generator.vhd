-- *****************************************************************************
-- * Project:   game
-- * Entity:    MessageGenerator
-- * Author:    mohrorm@msoe.edu
-- * Date:      2 February 2020
-- * Provides:  An entity that outputs scrolling seven segment messages
-- *****************************************************************************

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.utils.all;

entity MessageGenerator is
generic(
    MESSAGES : StringArray;
    -- actual lengths of the messages
    LENGTHS  : IntegerArray
);
port(
    clk      : in     std_logic;
    -- reset counter and output when low
    enable   : in     std_logic;
    -- selects the current message from array
    index    : in     unsigned;
    seg7_out : buffer Seg7Array := (others => (others => '1'))
);
end entity;

architecture dataflow of MessageGenerator is
    -- current scrolling position of message
    signal counter   : unsigned(Log2I(MESSAGES(0)'length) downto 0);
    signal next_seg7 : Seg7;
begin

    MoveMessage : process(clk, enable)
    begin
        if enable = '0' then
            counter <= to_unsigned(1, counter'length);
            seg7_out <= (others => (others => '1'));
        elsif rising_edge(clk) then
            -- scroll the message left and append the next character
            seg7_out <= seg7_out(1 to seg7_out'right) & next_seg7;
            if counter = LENGTHS(to_integer(index)) then
                -- wrap the message
                counter <= to_unsigned(1, counter'length);
            else
                counter <= counter + 1;
            end if;
        end if;
    end process;

    -- character LUT
    with MESSAGES(to_integer(index))(to_integer(counter)) select next_seg7 <=
        --numbers
        "00000011" when '0',
        "10011111" when '1',
        "00100101" when '2',
        "01100011" when '3',
        "10011001" when '4',
        "01001001" when '5',
        "01000001" when '6',
        "00011111" when '7',
        "00000001" when '8',
        "00011001" when '9',
        --lowercase
        "00000101" when 'a',
        "11000001" when 'b',
        "11100101" when 'c',
        "10000101" when 'd',
        "00100001" when 'e',
        "01110001" when 'f',
        "00001001" when 'g',
        "11010001" when 'h',
        "11110011" when 'i',
        "10000111" when 'j',
        "01010001" when 'k',
        "11100011" when 'l',
        -- 'm' = "rn"
        "11010101" when 'n',
        "11000101" when 'o',
        "00110001" when 'p',
        "00011001" when 'q',
        "11110101" when 'r',
        "01001001" when 's',
        "11100001" when 't',
        "11000111" when 'u' | 'v',
        --w must be followed by u
        "11100111" when 'w',
        --x not happening
        "10001001" when 'y',
        "00100101" when 'z',

        "11111111" when ' ',
        "11111110" when others;

end architecture;
