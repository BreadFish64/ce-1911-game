-- *****************************************************************************
-- * Project:   game
-- * Entity:    WallGen
-- * Author:    mohrorm@msoe.edu
-- * Date:      2 February 2020
-- * Provides:  An entity that outputs a stream of 3 bits with an uninterrupted
-- *            path of zeros representing walls
-- *****************************************************************************

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.utils.all;

entity WallGen is
port(
    clk    : in     std_logic;
    -- scroll and generate new walls
    enable : in     std_logic;
    -- clear the walls
    clr    : in     std_logic;
    -- number of clock cycles per generation
    speed  : in     unsigned;
    walls  : buffer WallArray := (others => (others => '0'))
);
end entity;

architecture behavioral of WallGen is
    signal counter   : unsigned(speed'range) := (others => '0');
    -- random number from 0 to 2 generated every two cycles
    signal rand      : unsigned(1 downto 0);
    -- holds the last position the zero path moved to
    signal path      : Wall := (others => '0');
    -- two out of three bits rotated by a random ammount
    -- determines where the path of zeros moves next
    signal mixer     : Wall;
    -- determines whether to move the path of zeros in the next cycle
    -- or hold the position
    -- if the path moves every cycle the output looks too empty
    signal hold_path : std_logic := '0';
begin
    rand_gen : entity work.XORShift generic map(MOD_NUM => Wall'length)
        port map(clk => hold_path, rand => rand);

    mixer <= Wall(unsigned'("011") ror to_integer(rand));

    WallGeneration : process(clk, clr) is
        variable prev_wall : Wall;
        variable next_wall : Wall;
    begin

        if clr then
            walls <= (others => (others => '0'));
            counter <= (others => '0');
        elsif rising_edge(clk) and enable = '1' then
            if counter = speed then
                counter <= (others => '0');
                if hold_path = '0' then
                    next_wall(0) := (path(0) and mixer(0));
                    next_wall(2) := (path(2) and mixer(2));
                    next_wall(1) := (next_wall(0) and next_wall(2)) and mixer(1);
                    path <= mixer;
                else
                    next_wall := path;
                end if;
                walls <= walls(1 to walls'right) & next_wall;
                hold_path <= not hold_path;
            else
                counter <= counter + 1;
            end if;
        end if;
    end process;

end architecture;
