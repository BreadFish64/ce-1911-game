-- *****************************************************************************
-- * Project:   game
-- * Package:   utils
-- * Author:    mohrorm@msoe.edu
-- * Date:      2 February 2020
-- * Provides:  Utility types and functions used throughout the project
-- *****************************************************************************


library ieee;
use ieee.std_logic_1164.all;

package utils is

-- contains a single seven segment character
subtype Seg7 is std_logic_vector(0 to 7);
type Seg7Array is array (natural range <>) of Seg7;

-- represents a wall
subtype Wall is std_logic_vector(2 downto 0);
type WallArray is array (natural range <>) of Wall;

type StringArray is array (natural range <>) of String;
type IntegerArray is array (natural range <>) of integer;

-- function that returns logbase2 of integer x
-- useful to get the high of the range necessary to store x
pure function Log2I(x : in natural) return natural;

end package;

package body utils is

pure function Log2I(x : in natural) return natural is
    variable log_result : natural := 0;
begin
    for i in 0 to 30 loop
        if x > 2**i then
            log_result := i;
        end if;
    end loop;
    return log_result;
end function;

end package body;
