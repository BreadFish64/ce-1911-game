-- *****************************************************************************
-- * Project:   game
-- * Entity:    Game
-- * Author:    mohrorm@msoe.edu
-- * Date:      2 February 2020
-- * Provides:  Game structure
-- *****************************************************************************

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;

use work.utils.all;

entity Game is
port(
    -- switch between game and instructions
    mode_switch  : in  std_logic;
    -- player movement input
    up, down     : in  std_logic;
    -- 50 MHz clock
    clk          : in  std_logic;
    -- display
    seg7_display : out Seg7Array(0 to 5);
    -- LEDs
    progress     : out std_logic_vector(0 to 9)
);
end entity;

architecture structural of Game is
    type GameState is (INSTRUCTIONS, PLAYING, LOST, LOSE_SCREEN, WIN_SCREEN);

    constant CLOCK_SPEED_HZ : positive := 4;
    -- cycles before speeding up
    constant LEVEL_UP_TIME  : positive := 20 * CLOCK_SPEED_HZ;
    -- number of cycles per wall movement
    constant STARTING_SPEED : natural := 3;
    constant ENDING_SPEED   : natural := 1;

    signal slow_clk : std_logic;

    signal game_state : GameState := INSTRUCTIONS;
    signal wall_speed : unsigned(Log2I(STARTING_SPEED) downto 0) :=
        to_unsigned(STARTING_SPEED, Log2I(STARTING_SPEED) + 1);
    signal counter    : unsigned(Log2I(LEVEL_UP_TIME) downto 0) :=
                                                (others => '0');

    signal walls          : WallArray(seg7_display'range);
    signal enable_walls   : std_logic;
    signal clear_walls    : std_logic;
    signal player         : Wall;
    signal encoded_walls  : Seg7Array(seg7_display'range);
    signal encoded_player : Seg7;
    -- true if the player has hit a wall
    signal died           : std_logic := '0';

    constant instructions_message : String := "flip toggle 0 to play. press " &
        "the buttons to rnove the player and dodge the wualls. survive for " &
        "60 seconds to wuin. ";
    constant win_message  : String := "congratulations. you dodged the wualls." &
        " flip the toggle to play again. ";
    constant lose_message : String :=
        "oops. you hit a wuall. flip the toggle to play again. ";
    signal message_index  : unsigned(1 downto 0);
    signal message        : Seg7Array(seg7_display'range);
    signal enable_message : std_logic;

begin

    Run : process(slow_clk) is
    begin
        if rising_edge(slow_clk) then
            if game_state = PLAYING then
                if died then
                    -- put 4 seconds on the counter to wait before showing
                    -- death screen
                    counter <= to_unsigned(4 * CLOCK_SPEED_HZ, counter'length);
                    game_state <= LOST;
                elsif counter = LEVEL_UP_TIME then
                    -- increase speed
                    counter <= (others => '0');
                    if wall_speed /= ENDING_SPEED then
                        wall_speed <= wall_speed - 1;
                    else
                        game_state <= WIN_SCREEN;
                    end if;
                else
                    counter <= counter + 1;
                end if;
            end if;
            if game_state = LOST then
                -- wait before showing death screen
                if counter /= 0 then
                    counter <= counter - 1;
                else
                    game_state <= LOSE_SCREEN;
                end if;
            end if;
            if game_state = LOSE_SCREEN then
                message_index <= to_unsigned(2, message_index'length);
                enable_message <= '1';
            end if;
            if game_state = WIN_SCREEN then
                message_index <= to_unsigned(1, message_index'length);
                enable_message <= '1';
            end if;
            if game_state = INSTRUCTIONS then
                -- reset game state
                wall_speed <= to_unsigned(STARTING_SPEED, wall_speed'length);
                counter <= (others => '0');
                message_index <= to_unsigned(0, message_index'length);
                enable_message <= '1';
                if mode_switch = '1' then
                    -- start game
                    enable_message <= '0';
                    game_state <= PLAYING;
                end if;
            end if;
            if mode_switch = '0' and game_state /= INSTRUCTIONS then
                -- ensure message is cleared when transitioning states
                enable_message <= '0';
                game_state <= INSTRUCTIONS;
            end if;
        end if;
    end process;

    -- intentionally runs at a higher clock rate
    -- handling the player asynchronously introduced bugs
    PlayerMovement : process(clk) is
    begin
        if rising_edge(clk) then
            if died = '0' then
                -- don't move the player if they have died
                -- buttons are active low
                -- if both buttons are pressed, place the player in the center
                player <= (down and not up, up xnor down, up and not down);
            end if;
            if or_reduce(walls(0) and player) = '1' then
                -- the player has collided with a wall
                died <= '1';
            end if;
            if game_state = INSTRUCTIONS then
                -- reset death state
                died <= '0';
            end if;
        end if;
    end process;

    enable_walls <= '1' when game_state = PLAYING else '0';

    with game_state select clear_walls <=
        '0' when PLAYING | LOST,
        '1' when others;

    wall_generator : entity work.WallGen port map(
        clk => slow_clk,
        enable => enable_walls,
        clr => clear_walls,
        speed => wall_speed,
        walls => walls
    );

    wall_encoder : entity work.WallEncoder port map(walls, encoded_walls);

    -- encodes blinking player
    encoded_player <= (slow_clk or not player(2)) & "11" &
        (slow_clk or not player(0)) & "11" & (slow_clk or not player(1)) & '1';

    -- show progress indicator while in game
    progress <= std_logic_vector(
        -- end marker
        (unsigned'('1' & (progress'length - 2 downto 0 => '0')) srl
        STARTING_SPEED - ENDING_SPEED + 1) or
        -- current progress
        (unsigned'(slow_clk & (progress'length - 2 downto 0 => '0')) srl
        STARTING_SPEED - ENDING_SPEED - to_integer(wall_speed - ENDING_SPEED)))
        when game_state = PLAYING
        else (others => '0');

    with game_state select seg7_display <=
        message when INSTRUCTIONS | WIN_SCREEN | LOSE_SCREEN,
        -- mix player and walls for output
        (encoded_walls(0) xnor encoded_player) &
        encoded_walls(1 to seg7_display'length - 1) when others;

    message_gen : entity work.MessageGenerator generic map(
        MESSAGES => (
            instructions_message,
            -- VHDL doesn't allow jagged arrays
            -- so strings must be padded to the same length
            win_message &
                (1 to instructions_message'length - win_message'length => ' '),
            lose_message &
                (1 to instructions_message'length - lose_message'length => ' ')
        ),
        LENGTHS => (
            instructions_message'length,
            win_message'length,
            lose_message'length
        )
    ) port map (
        clk => slow_clk,
        enable => enable_message,
        index => message_index,
        seg7_out => message
    );

    slow_clock : entity work.SlowClock generic map(real(CLOCK_SPEED_HZ))
        port map('0', clk, slow_clk);

end architecture;
