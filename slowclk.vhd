-- *************************************************************
-- * FILENAME: slowclock.vhd *
-- * PROVIDES: *
-- * - High frequency clocks prevent the use of LEDs to see *
-- * output signals changing because human eyes only resolve *
-- * blinking light between 0 and approximately 30Hz. *
-- * - This file creates a slow 1 second clock by recognizing *
-- * that the 50MHz clock is pulsing 50E6 times per second. *
-- * A one second clock would pulse 1 time per second. *
-- * Thus, the one second pulse is high for 25E6 fast clocks *
-- * and low for 25E6 fast clocks. *
-- *************************************************************
-- * TO USE: *
-- * - Add this component into the clock path of an FSM under *
-- * test. Connect the 50MHz clock to the input named CLK50 *
-- * and connect output CLK1 to the machine clock input. *
-- * - In Quartus, use structural VHDL to complete this port *
-- * mapping or use a schematic blueprint that drops both *
-- * this component and the machine component in as the top *
-- * level entity. *
-- * - DO NOT ATTEMPT TO SIMULATE when this module is in place *
-- * because it will take 50 million clock periods to get *
-- * one clock period on your machine. Your computer will *
-- * take forever to complete a simulation and consume much *
-- * hard drive space. Only simulate your machine before you *
-- * add this component to the clock path. *
-- *************************************************************
library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.utils.all;

entity SlowClock is
generic(
    CLOCK_SPEED_HZ : real := 1.0
);
port( 
    rst, clk50MHz : in     std_logic;
    clk_out       : buffer std_logic
);
end entity;

architecture behavioral of SlowClock is
    constant HALF : natural := 
        natural(50000000.0 / 2.0 / CLOCK_SPEED_HZ);
    signal count  : unsigned(Log2I(HALF) downto 0) := 
        (others =>'0');
begin

    Update : process(rst, clk50MHz)
    begin
        if rst = '1' then
            count <= (others =>'0');
        elsif rising_edge(clk50MHz) then 
            count <= count + 1;
            if count = HALF then 
                clk_out <= not clk_out;
                count <= (others =>'0');
            end if;
        end if;
    end process;

end architecture BEHAVIORAL;