-- *****************************************************************************
-- * Project:   game
-- * Entity:    XORShift
-- * Author:    mohrorm@msoe.edu
-- * Date:      2 February 2020
-- * Provides:  A basic XORShift random number generator
-- *****************************************************************************

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.utils.all;

entity XORShift is
generic(
    -- wrap value
    MOD_NUM : positive := positive'high;
    -- initial value
    SEED    : natural  := 32845691
);
port(
    clk  : in     std_logic;
    rand : buffer unsigned
);
end entity;

architecture behavioral of XORShift is
    signal q : unsigned(31 downto 0) := to_unsigned(SEED, 32);
begin

    Randomize : process(clk) is
        variable x : unsigned(31 downto 0) := q;
    begin
        if rising_edge(clk) then
            x := q;
            x := x xor (x sll 13);
            x := x xor (x srl 17);
            x := x xor (x sll 5);
            q <= x;
            x := x mod MOD_NUM;
            rand <= x(rand'length - 1 downto 0);
        end if;
    end process;

end architecture behavioral;
