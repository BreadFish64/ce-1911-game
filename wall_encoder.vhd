-- *****************************************************************************
-- * Project:   game
-- * Entity:    WallEncoder
-- * Author:    mohrorm@msoe.edu
-- * Date:      2 February 2020
-- * Provides:  A seven segments encoder for 3-bit walls
-- *****************************************************************************

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.utils.all;

entity WallEncoder is
port(
    walls    : in  WallArray;
    seg7_out : out Seg7Array
);
end entity;

architecture dataflow of WallEncoder is
    constant WALL_ENCODINGS : Seg7Array(0 to 7) := (
        "11111111",
        "11101111",
        "11111101",
        "11010101",
        "01111111",
        "01101111",
        "10111001",
        "11110011"
    );
begin

    GenerateSeg7Output : for i in walls'range generate
        seg7_out(i) <= WALL_ENCODINGS(to_integer(unsigned(walls(i))));
    end generate;

end architecture;
